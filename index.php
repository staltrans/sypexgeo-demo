<!doctype html>
<html>
<head>
  <title>Пример работы с sypexgeo</title>
</head>
<body>
  <p><small><a href="https://bitbucket.org/staltrans/sypexgeo-demo">bitbucket</a> | <a href="sypexgeo.tar.gz">Исходный код</a></small></p>
  <p>
    На <a href="https://sypexgeo.net/ru/download/">странице загрузки</a> скачиваем <a href="https://sypexgeo.net/files/SxGeo22_API.zip">Sypex Geo для PHP 5.2+</a> и базы <a href="https://sypexgeo.net/files/SxGeoCountry.zip">Sypex Geo Country</a>, <a href="https://sypexgeo.net/files/SxGeoCity_utf8.zip">Sypex Geo City (UTF-8)</a>
  </p>
  <p>
    <form id="sypexgeo" method="post">
      <fieldset>
        <div>
          <label for="ip_address">IP Address:</label>
          <input name="ip_address" type="text" value="<?php echo ip_addr() ?>" placeholder="95.183.44.106" required>
        </div>
        <label for="db">Database:</label><br/>
        <input name="db" value="country" type="radio" /><a href="https://sypexgeo.net/files/SxGeoCountry.zip">Sypex Geo Country</a><br/>
        <input name="db" value="city" type="radio" checked="checked" /><a href="https://sypexgeo.net/files/SxGeoCity_utf8.zip">Sypex Geo City (UTF-8)</a><br/><br/>
        <input name="getinfo" type="submit" value="Submit">
      </fieldset>
    </form>
  </p>
  <p><pre><?php
    if (isset($_POST['ip_address']) && isset($_POST['db'])) {
      $ip = check_plain($_POST['ip_address']);
      switch(check_plain($_POST['db'])) {
        case 'country':
          $db = 'db/SxGeo.dat';
          break;
        case 'city':
          $db = 'db/SxGeoCity.dat';
          break;
        default:
          $db = NULL;
      }
      if (is_ip($ip))  {
        include('sxgeo/SxGeo.php');
        $SxGeo = new SxGeo($db, SXGEO_BATCH | SXGEO_MEMORY);
        echo "<strong>Метод getCountry($ip) = </strong><br/>";
        var_dump($SxGeo->getCountry($ip));
        echo "<br /><strong>Метод getCountryId($ip) = </strong><br/>";
        var_dump($SxGeo->getCountryId($ip));
        echo "<br /><strong>Метод getCity($ip) = </strong><br/>";
        var_dump($SxGeo->getCity($ip));
        echo "<br /><strong>Метод getCityFull($ip) = </strong><br/>";
        var_dump($SxGeo->getCityFull($ip));
        echo "<br /><strong>Метод get($ip) = </strong><br/>";
        var_dump($SxGeo->get($ip));
      }
    }
  ?></pre></p>
</body>
</html>

<?php

function check_plain($text) {
  return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function is_ip($ip) {
  return filter_var($ip, FILTER_VALIDATE_IP);
}

function ip_addr() {
  return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
}
